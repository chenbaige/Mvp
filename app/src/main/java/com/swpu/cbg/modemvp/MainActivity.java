package com.swpu.cbg.modemvp;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;

import com.swpu.cbg.modemvp.Bean.User;
import com.swpu.cbg.modemvp.Presenter.UserLoginPresenter;
import com.swpu.cbg.modemvp.View.IUserView;

public class MainActivity extends AppCompatActivity implements IUserView{

    private UserLoginPresenter mPresenter;
    private EditText mUserText,mPassWordText;
    private Button loginBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        initView();
    }

    private void initView() {
        mPresenter = new UserLoginPresenter(this, MainActivity.this);
        mUserText = (EditText) findViewById(R.id.id_name_text);
        mPassWordText = (EditText) findViewById(R.id.id_password_text);
        loginBtn = (Button) findViewById(R.id.id_login);
        loginBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.userLogin();
            }
        });
    }


    @Override
    public User getUser(){
        return new User(mUserText.getText().toString(),mPassWordText.getText().toString());
    }

    @Override
    public String getUserName() {
        return mUserText.getText().toString();
    }

    @Override
    public String getPassword() {
        return mPassWordText.getText().toString();
    }

    @Override
    public void showDialog() {

    }

    @Override
    public void DissmissDialog() {

    }
}
