package com.swpu.cbg.modemvp.View;

import com.swpu.cbg.modemvp.Bean.User;

/**
 * Created by chenboge on 16/4/28.
 */
public interface IUserView {

    User getUser();
    String getUserName();
    String getPassword();
    void showDialog();
    void DissmissDialog();
}
