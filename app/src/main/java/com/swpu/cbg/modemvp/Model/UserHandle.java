package com.swpu.cbg.modemvp.Model;

import com.swpu.cbg.modemvp.Bean.User;

/**
 * Created by chenboge on 16/4/28.
 */
public class UserHandle implements IUserHandle {

    @Override
    public void Login(final User user, final OnLoginListener mListener) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if(user.getUserName().equals("baigege")){
                   mListener.LoginSuccess(user);
                }else {
                    mListener.LoginFail();
                }
            }
        }).start();
    }
}
