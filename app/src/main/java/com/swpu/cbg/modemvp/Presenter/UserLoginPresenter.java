package com.swpu.cbg.modemvp.Presenter;

import android.content.Context;
import android.os.Handler;
import android.widget.Toast;

import com.swpu.cbg.modemvp.Bean.User;
import com.swpu.cbg.modemvp.Model.IUserHandle;
import com.swpu.cbg.modemvp.Model.OnLoginListener;
import com.swpu.cbg.modemvp.Model.UserHandle;
import com.swpu.cbg.modemvp.View.IUserView;

/**
 * Created by chenboge on 16/4/28.
 */
public class UserLoginPresenter {

    private Handler mHanldler=new Handler();
    private IUserView mView;
    private IUserHandle mHadler=new UserHandle();
    private Context mContext;

    public UserLoginPresenter(IUserView mView,Context mContext) {
        this.mView = mView;

        this.mContext=mContext;
    }



    public void userLogin(){
        mHadler.Login(mView.getUser(), new OnLoginListener() {
            @Override
            public void LoginSuccess(User user) {
                mHanldler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "success", Toast.LENGTH_SHORT).show();
                    }
                });

            }

            @Override
            public void LoginFail() {

                mHanldler.post(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(mContext, "fail", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }
}
