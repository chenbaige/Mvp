package com.swpu.cbg.modemvp.Model;

import com.swpu.cbg.modemvp.Bean.User;

/**
 * Created by chenboge on 16/4/28.
 */
public interface OnLoginListener {

    void LoginSuccess(User user);

    void LoginFail();
}
