package com.swpu.cbg.modemvp.Bean;

/**
 * Created by chenboge on 16/4/28.
 */
public class User {

    private String UserName;

    public User(String userName, String password) {
        UserName = userName;
        Password = password;
    }

    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    private String Password;

}
